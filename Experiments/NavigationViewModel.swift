//
//  NavigationViewModel.swift
//  Experiments
//
//  Created by M Ramdhan Syahputra on 28/11/23.
//

import Foundation
import SwiftUI

@Observable
class NavigationViewModel {
    var path: NavigationPath {
        didSet {
            debugPrint("Old value: \(oldValue.count), new value: \(path.count)")
            save()
        }
    }
    
    private let savePath = URL.documentsDirectory.appending(path: "SavedPath")
    
    init() {
        if let data = try? Data(contentsOf: savePath) {
            if let decoded = try? JSONDecoder().decode(NavigationPath.CodableRepresentation.self, from: data) {
                self.path = NavigationPath(decoded)
                return
            }
        }
        
        self.path = NavigationPath()
    }
    
    func save() {
        guard let representation = path.codable else { return }
        
        do {
            let data = try JSONEncoder().encode(representation)
            try data.write(to: savePath)
        } catch {
            print("Failed to save navigation data")
        }
    }
}

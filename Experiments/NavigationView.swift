//
//  NavigationView.swift
//  Experiments
//
//  Created by M Ramdhan Syahputra on 28/11/23.
//

import SwiftUI

struct NavigationView: View {
    @State private var viewModel = NavigationViewModel()
    
    var body: some View {
        NavigationStack(path: $viewModel.path) {
            TabView {
                Button("Click here") {
                    viewModel.path.append(1)
                }
                .tabItem {
                    Label("Home", systemImage: "house")
                }
                
                
                DetailStoryView()
                    .tabItem {
                        Label("Story", systemImage: "paperplane")
                    }
                
                Text("Settings page")
                    .tabItem {
                        Label("Story", systemImage: "gear")
                    }
            }
            .navigationTitle("Home")
            .navigationBarTitleDisplayMode(.inline)
            .toolbarBackground(.red)
            .toolbarColorScheme(.dark)
            .navigationDestination(for: Int.self) { number in
                DetailNavigationView(number: number, path: $viewModel.path)
            }
        }
    }
}

struct DetailStoryView: View {
    var body: some View {
        NavigationStack {
            NavigationLink {
                Text("Detail of story")
            } label: {
                Text("Click story here")
            }
            .navigationTitle("Story")
        }
    }
}

struct DetailNavigationView: View {
    var number: Int
    @Binding var path: NavigationPath
    
    var body: some View {
        VStack {
            Text("Halaman \(number)")
            NavigationLink("Klik disini", value: Int.random(in: 0...999))
                .toolbar {
                    Button("Home") {
                       path = NavigationPath()
                    }
                }
        }
    }
}

#Preview {
    NavigationView()
}

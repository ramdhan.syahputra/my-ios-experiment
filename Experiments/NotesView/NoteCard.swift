//
//  NoteCard.swift
//  Experiments
//
//  Created by M Ramdhan Syahputra on 24/11/23.
//

import SwiftUI

struct NoteCard: View {
    let title: String
    let subtitle: String
    let date: Date
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(title)
                    .font(.headline)
                Text(subtitle)
                    .font(.subheadline)
            }
            Spacer()
            Text(date.formatted(date: .complete, time: .omitted))
                .font(.caption2)
        }
    }
}

#Preview {
    let dummy = NoteModel.dummyNote
    return NoteCard(title: dummy.title, subtitle: dummy.subtitle, date: dummy.date)
}

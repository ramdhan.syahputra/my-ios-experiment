//
//  AddNoteView.swift
//  Experiments
//
//  Created by M Ramdhan Syahputra on 24/11/23.
//

import SwiftUI

struct AddNoteView: View {
    @State private var title = ""
    @State private var subtitle = ""
    @State private var date = Date.now
    @Environment(\.dismiss) private var dismiss
    @State private var viewModel = NotesViewModel()
    
    var body: some View {
        NavigationStack {
            Form {
                TextField("Title", text: $title)
                TextField("Subtitle", text: $subtitle, axis: .vertical)
                    .lineLimit(10, reservesSpace: true)
                DatePicker("Date", selection: $date, displayedComponents: .date)
                    .datePickerStyle(.compact)
            }
            .toolbar {
                ToolbarItem(placement: .topBarTrailing) {
                    if title.isEmpty || subtitle.isEmpty {
                        Button("Close") {
                            dismiss()
                        }
                    }
                    Button("Done") {
                        let note = NoteModel(id: UUID(), title: title, subtitle: subtitle, date: date)
                        viewModel.addNote(note)
                        dismiss()
                    }
                }
            }
        }
    }
}

#Preview {
    AddNoteView()
}

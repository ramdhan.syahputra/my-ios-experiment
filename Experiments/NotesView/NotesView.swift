//
//  NotesView.swift
//  Experiments
//
//  Created by M Ramdhan Syahputra on 24/11/23.
//

import SwiftUI

struct NotesView: View {
    @State private var query = ""
    @State private var isSheetPresented = false
    @State private var viewModel = NotesViewModel()
    
    var body: some View {
        NavigationStack {
            List {
                ForEach(viewModel.notes) { note in
                    NoteCard(
                        title: note.title, subtitle: note.subtitle, date: note.date)
                }
                .onDelete(perform: viewModel.removeNote)
            }
            .listStyle(.plain)
            .navigationTitle("Notes")
            .searchable(text: $query)
            .toolbar {
                ToolbarItem(placement: .topBarLeading) {
                    EditButton()
                }
                ToolbarItem(placement: .topBarTrailing) {
                    Button("Add") {
                        isSheetPresented = true
                    }
                }
            }
            .sheet(isPresented: $isSheetPresented) {
                AddNoteView()
            }
        }
    }
}

#Preview {
    NotesView()
}

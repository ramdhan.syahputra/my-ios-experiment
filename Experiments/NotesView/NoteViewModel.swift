//
//  NoteViewModel.swift
//  Experiments
//
//  Created by M Ramdhan Syahputra on 24/11/23.
//

import Foundation

@Observable
class NotesViewModel {
    private let notesKey = "NOTES_DATA"
    
    var notes = [NoteModel]()
    
    func addNote(_ note: NoteModel) {
        debugPrint("Added \(note)")
        notes.append(note)
    }
    
    func removeNote(_ indexSet: IndexSet) {
        notes.remove(atOffsets: indexSet)
    }
    
    func editNote(oldNote: NoteModel, newNote: NoteModel) {
        if let indexNote = notes.firstIndex(where: { note in note.id == oldNote.id }) {
            notes[indexNote] = newNote
        }
    }
}

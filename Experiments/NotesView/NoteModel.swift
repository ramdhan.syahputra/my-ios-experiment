//
//  NoteModel.swift
//  Experiments
//
//  Created by M Ramdhan Syahputra on 24/11/23.
//

import Foundation

struct NoteModel: Codable, Identifiable {
    var id: UUID
    let title: String
    let subtitle: String
    let date: Date
    
    static var dummyNote = NoteModel(id: UUID(), title: "Title", subtitle: "Subtitle", date: Date.now)
    
    static var dummyNotes = [
        NoteModel(id: UUID(), title: "Title1", subtitle: "Subtitle", date: Date.now),
        NoteModel(id: UUID(), title: "Title2", subtitle: "Subtitle", date: Date.now),
        NoteModel(id: UUID(), title: "Title3", subtitle: "Subtitle", date: Date.now),
        NoteModel(id: UUID(), title: "Title4", subtitle: "Subtitle", date: Date.now),
        NoteModel(id: UUID(), title: "Title5", subtitle: "Subtitle", date: Date.now)
    ]
}

//
//  ExperimentsApp.swift
//  Experiments
//
//  Created by M Ramdhan Syahputra on 23/11/23.
//

import SwiftUI

@main
struct ExperimentsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentViewNavigation()
        }
    }
}

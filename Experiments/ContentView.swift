//
//  ContentView.swift
//  Experiments
//
//  Created by M Ramdhan Syahputra on 23/11/23.
//

import SwiftUI

struct ContentView: View {
    @State private var currentTab = 4
    var body: some View {
        TabView(selection: $currentTab) {
            Text("Home page")
                .tabItem {
                    Label("Status", systemImage: "circle.dashed")
                        .environment(\.symbolVariants, .none)
                }
                .tag(1)
            CallsView()
                .tabItem {
                    Label("Calls", systemImage: "phone")
                        .environment(\.symbolVariants, .none)
                }
                .tag(2)
            Text("Communities page")
                .tabItem {
                    Label("Communities", systemImage: "person.3")
                        .environment(\.symbolVariants, .none)
                }
                .tag(3)
            ChatsView()
                .tabItem {
                    Label("Chats", systemImage: "bubble.left.and.bubble.right")
                        .environment(\.symbolVariants, .none)
                }
                .tag(4)
            SettingsView()
                .tabItem {
                    Label("Settings", systemImage: "gear.circle")
                        .environment(\.symbolVariants, .none)
                }
                .tag(5)
        }
    }    
}

#Preview {
    ContentView().preferredColorScheme(.dark)
}

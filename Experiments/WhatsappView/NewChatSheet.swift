//
//  NewChatSheet.swift
//  Experiments
//
//  Created by M Ramdhan Syahputra on 23/11/23.
//

import SwiftUI

struct NewChatSheet: View {
    @State private var users = ["Ramdhan", "Firman", "Adisti", "Adinda", "Sheila"]
    @State private var userSelected = "Ramdhan"
    @State private var message = ""
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        NavigationStack {
            Form {
                Section {
                    Picker("Users", selection: $userSelected) {
                        ForEach(users, id: \.self) { user in
                            Text(user)
                        }
                    }.pickerStyle(.navigationLink)
                    
                    TextField("Type message...", text: $message, axis: .vertical)
                        .lineLimit(20, reservesSpace: true)
                } header: {
                    Text("Form")
                }
            }
            .toolbar {
                ToolbarItem(placement: .topBarTrailing) {
                    if message.isEmpty {
                        Button("Close") {
                            dismiss()
                        }
                    }
                    Button("Done") {
                        debugPrint("Do something")
                    }
                }
            }
        }
    }
}

#Preview {
    NewChatSheet()
}

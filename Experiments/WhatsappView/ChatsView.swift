//
//  ChatsView.swift
//  Animations
//
//  Created by M Ramdhan Syahputra on 22/11/23.
//

import SwiftUI

struct ChatsView: View {
    @State private var users = ["Ramdhan", "Lakdar", "Gian", "Adi", "Andres", "Taufik"]
    
//    @AppStorage("name") private var name = "Ramdhan"
    
    @State private var isSheetPresented = false
    @State private var query = ""
    
    var filteredUsers: [String] {
        if query.isEmpty {
            return users
        } else {
            return users.filter {
                $0.localizedCaseInsensitiveContains(query)
            }
        }
    }
    
    var body: some View {
        NavigationStack {
            List {
                ForEach(filteredUsers, id: \.self) { user in
                    HStack {
                        Image(systemName: "photo")
                            .resizable()
                            .scaledToFill()
                            .frame(width: 50, height: 50)
                            .clipShape(.circle)
                        VStack(alignment: .leading, spacing: 1) {
                            HStack {
                                Text(user)
                                    .font(.headline)
                                Spacer()
                                Text("6:15 pm")
                                    .font(.caption)
                                    .foregroundStyle(.secondary)
                            }
                            Text("New message")
                                .font(.subheadline)
                                .foregroundStyle(.secondary)
                            
                        }
                    }
                    .padding(.horizontal, 8)
                    .padding(.vertical, 12)
                }
                .onDelete(perform: deleteUser)
                .onMove(perform: { index, offset in
                    users.move(fromOffsets: index, toOffset: offset)
                })
                .listRowSeparator(.hidden)
            }
            .listStyle(.plain)
            .navigationTitle("Chats")
            .searchable(text: $query.animation(.spring))
            .sheet(isPresented: $isSheetPresented) {
                NewChatSheet()
            }
            .toolbar {
                ToolbarItem(placement: .topBarTrailing) {
                    Button("Edit", systemImage: "square.and.pencil") {
                        isSheetPresented = true
                    }
                }
                
                ToolbarItem(placement: .topBarLeading) {
                    EditButton()
                }
            }
        }
    }
    
    func deleteUser(of index: IndexSet) {
        users.remove(atOffsets: index)
    }
}

#Preview {
    ChatsView()
}

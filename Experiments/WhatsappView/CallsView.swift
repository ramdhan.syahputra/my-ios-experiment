//
//  CallsView.swift
//  Animations
//
//  Created by M Ramdhan Syahputra on 22/11/23.
//

import SwiftUI

struct CallsView: View {
    @State private var selectedOption = "All"
    @State private var options = ["All", "Dismissed"]
    
    var body: some View {
        VStack {
            NavigationStack {
                List {
                    ForEach(1..<20, id: \.self) { _ in
                        Label(
                            title: { Text("Label") },
                            icon: { Image(systemName: "42.circle") }
                        )
                    }
                }
                .navigationTitle("Calls")
                .searchable(text: .constant(""))
                .toolbar {
                    ToolbarItem(placement: .principal) {
                        VStack {
                            Picker("", selection: $selectedOption) {
                                ForEach(options, id:\.self) {option in
                                    Text(option)
                                }
                            }
                            .pickerStyle(.segmented)
                        }
                    }
                    
                    ToolbarItem(placement: .topBarLeading) {
                        Button("Edit") {
                            
                        }
                    }
                    
                    ToolbarItem(placement: .topBarTrailing) {
                        Button {
                            
                        } label: {
                            Label(
                                title: { Text("Label") },
                                icon: { Image(systemName: "phone") }
                            )
                        }
                    }
                }
            }
        }
    }
}

#Preview {
    CallsView()
}

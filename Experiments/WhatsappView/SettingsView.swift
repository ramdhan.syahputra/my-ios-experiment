//
//  SettingsView.swift
//  Animations
//
//  Created by M Ramdhan Syahputra on 22/11/23.
//

import SwiftUI

struct SettingsView: View {
    var body: some View {
        NavigationStack {
            List {
                Section {
                    HStack {
                        Image(systemName: "photo")
                            .resizable()
                            .scaledToFill()
                            .frame(width: 55, height: 55)
                            .clipShape(.circle)
                            .padding(.trailing, 8)
                        
                        VStack(alignment: .leading) {
                            Text("Ramdhan")
                                .font(.headline)
                            Text("Si vis amari ama")
                                .font(.subheadline)
                        }
                        
                        Spacer()
                        
                        Circle()
                            .fill(.secondary.opacity(0.2))
                            .frame(width: 40)
                            .overlay {
                                Image(systemName: "barcode.viewfinder")
                                    .overlay {
                                        
                                    }
                            }
                    }
                    
                    NavigationLink {
                        Text("Detail")
                    } label: {
                        Label(
                            title: { Text("Avatar") },
                            icon: { Image(systemName: "42.circle") }
                        )
                    }
                }
                .listRowSeparator(.visible)
                
                Section {
                    ForEach(1..<4, id: \.self) { num in
                        Label(
                            title: { Text("Setting") },
                            icon: { Image(systemName: "\(num).circle") }
                        )
                    }
                }
                
                Section {
                    ForEach(1..<6, id: \.self) { num in
                        Label(
                            title: { Text("Setting") },
                            icon: { Image(systemName: "\(num).circle") }
                        )
                    }
                }
                
                Section {
                    ForEach(1..<3, id: \.self) { num in
                        Label(
                            title: { Text("Setting") },
                            icon: { Image(systemName: "\(num).circle") }
                        )
                    }
                }
            }
            .navigationTitle("Settings")
            .searchable(text: .constant(""))
        }
    }
}

#Preview {
    SettingsView().preferredColorScheme(.dark)
}

//
//  ContentViewNavigation.swift
//  Experiments
//
//  Created by M Ramdhan Syahputra on 28/11/23.
//

import SwiftUI

struct ContentViewNavigation: View {
    var body: some View {
        NavigationView()
    }
}

#Preview {
    ContentViewNavigation()
}
